CREATE TABLE IF NOT EXISTS  `ip_tracing` (
    `id` serial PRIMARY KEY,
    `ip_address` VARCHAR (255) NOT NULL,
    `user_agent` VARCHAR (255) NOT NULL,
    `view_date` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `page_url` VARCHAR (255) NOT NULL,
    `views_count` int
);

CREATE INDEX ip_tracing_ip
ON ip_tracing (ip_address);
