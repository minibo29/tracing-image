<?php

const DB_HOST = 'mysql';
const DB_PORT = '3306';
const DB_NAME = 'banner';
const DB_USER = 'root';
const DB_PASS = 'root';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

const FILE_PATH = 'banner.jpeg';

run();

/**
 * @throws \Exception
 */
function run()
{
    if (!upsertCount()) {
        throw new Exception("Error in data insert!");
    }

    printFile(FILE_PATH);
}

/**
 * @throws \Exception
 */
function printFile($filename)
{

    if (!file_exists($filename)) {
        throw new Exception("File not exist!!");
    }

    $file_extension = strtolower(substr(strrchr($filename, "."), 1));

    switch ($file_extension) {
        case "gif":
            $ctype = "image/gif";
            break;
        case "png":
            $ctype = "image/png";
            break;
        case "jpeg":
        case "jpg":
            $ctype = "image/jpeg";
            break;
        case "svg":
            $ctype = "image/svg+xml";
            break;
        default:
    }

    header('Content-type: ' . $file_extension);
    header("Content-Length: " . filesize($filename));

    readfile($filename);
    exit();
}

function upsertCount(): bool
{
    list($ip_address, $user_agent, $page_url) = getData();

    $connection = getConnection();

    list($id, $views_count) = getTracing($connection, $ip_address, $user_agent, $page_url);

    if ($id) {
        return updateViewsCount($connection, $id, ++$views_count);
    }

    return insertTracing($connection, $ip_address, $user_agent, $page_url);
}

function getData(): array
{
    return [getIp(), getAgent(), getUrl()];
}

function getAgent()
{
    return $_SERVER['HTTP_USER_AGENT'] ?? 'unknown';
}

function getIp()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    return $ip;
}

function getUrl(): string
{
    return trim($_SERVER['HTTP_REFERER'] ?? $_SERVER['REQUEST_URI'], '/');
}

function getConnection(): PDO
{
    static $connection;

    if ($connection) {
        return $connection;
    }

    try {
        $connection = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }

    return $connection;
}

function getTracing($pdo, $ip_address, $user_agent, $page_url): array
{
    $stmt = $pdo->prepare("SELECT id, views_count FROM ip_tracing where ip_address = :ip_address and user_agent = :user_agent  and page_url = :page_url ");
    $stmt->execute([
        'ip_address' => $ip_address,
        'user_agent' => $user_agent,
        'page_url' => $page_url,
    ]);
    $row = $stmt->fetch();

    return [$row['id'] ?? null, $row['views_count'] ?? null];
}

function insertTracing($pdo, $ip_address, $user_agent, $page_url): bool
{
    $stmt = $pdo->prepare("INSERT INTO ip_trascing (ip_address, user_agent, page_url, views_count)
        VALUES (:ip_address, :user_agent, :page_url, 1)");
    return $stmt->execute([
        'ip_address' => $ip_address,
        'user_agent' => $user_agent,
        'page_url' => $page_url,
    ]);
}

function updateViewsCount($pdo, $id, $views_count): bool
{
    $stmt = $pdo->prepare("UPDATE ip_tracing SET views_count = :views_count where id = :id");
    return $stmt->execute([
        'id' => $id,
        'views_count' => $views_count,
    ]);
}
